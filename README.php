<?php
$conexion = new mysqli("localhost", "root", "", "liga");
if ($conexion->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
}else{
  $resultado = $conexion->query("SELECT * FROM equipo");
}
 ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <style>
    input[type=submit]{
      margin-bottom: 1em;
      width: 6em;
      height: 3em;
      background: #27ae60;
      border-radius: 5px;
      border: solid;
      color: white;
    }
    </style>
  </head>
  <body>
    <div class="container">
      <nav>
        <div class="nav-wrapper">
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="topsalario.php">Top Salario</a></li>
            <li><a href="topalturas.php">Top 5 alturas</a></li>
          </ul>
        </div>
      </nav>
      <h2>Equipos</h2>
      <table>
        <tr>
          <th>id</th>
          <th>Nombre</th>
          <th>Ciudad</th>
          <th>web</th>
        </tr>
        <?php
       
          $resultado = $conexion->query("SELECT * FROM equipo");
$jeje = 0;
          foreach ($resultado as $equipo) {
            echo "<tr>";
            echo "<td>"."<a href='mostrarequipo.php?id_equipo=".$jeje."'>".$equipo['id_equipo']."</a></td>";
            echo "<td>".$equipo['nombre']."</td>";
            echo "<td>".$equipo['ciudad']."</td>";
            echo "<td>".$equipo['web']."</td>";
            echo "</tr>";
              $jeje++;

          }
          
        ?>
      </table>

      <h2>INSERTAR EQUIPO</h2>

      <form method="GET" action="insertarEquip.php">
        Nombre:<br>
        <input type="text" name="nombre"><br>
        Ciudad:<br>
        <input type="text" name="ciudad"><br>
        Web:
        <input type="text" name="web"><br><br>
        <input type="submit" value="enviar">
      </form>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>